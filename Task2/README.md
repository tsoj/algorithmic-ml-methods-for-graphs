# Runs

```bash
conda create -n graph_ml  python=3.9
conda activate graph_ml

pip install graphviz jupyterlab networkit networkx
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu
pip install torch-geometric
pip install torch-cluster -f https://data.pyg.org/whl/torch-2.3.0+cpu.html

python main.py
```
import torch_geometric.data
from torch_geometric.utils import from_networkit
from torch_geometric import EdgeIndex
from torch_geometric.data import Data
from torch.distributions import Geometric
from torch_cluster import random_walk
import networkit as nk
import math
import torch
import random


def get_incoming_neighbors(edge_index, num_nodes):
    # Create an empty list for each node
    incoming_edges = {node: [] for node in range(num_nodes)}

    # Iterate over the edge_index to populate the incoming_edges
    for source, target in edge_index.t():
        incoming_edges[target.item()].append(source.item())

    return incoming_edges


def calculate_out_degrees(edge_index, num_nodes):
    # Initialize a tensor to hold the out-degree of each node
    out_degrees = torch.zeros(num_nodes, dtype=torch.long)

    # Count the occurrences of each node in the first row of edge_index
    for source_node in edge_index[0]:
        out_degrees[source_node] += 1

    return out_degrees


def geometric_random_walk(edge_index: EdgeIndex, num_nodes, prob_success, start_node):
    # Sample a length for the random walk from a geometric distribution
    geom_dist = Geometric(prob_success)
    walk_length = max(
        1, geom_dist.sample().round().int().item()
    )  # Convert to Python int

    # Perform the random walk
    row, col = edge_index
    # print(walk_length)
    walk = random_walk(
        row=row,
        col=col,
        start=torch.tensor([start_node], dtype=torch.long),
        walk_length=walk_length,
    )
    assert walk.shape[0] == 1
    return walk[0].tolist()


class PPR:
    def __init__(self, G: EdgeIndex, alpha, delta):
        self.alpha = alpha
        self.delta = delta
        self.c = 350
        self.beta = 1 / 6
        self.e_rev = math.sqrt(delta)

        self.g_edge_index = G
        assert self.g_edge_index.num_cols == self.g_edge_index.num_rows
        self.num_nodes = self.g_edge_index.num_cols
        self.incoming_neighbors = get_incoming_neighbors(
            self.g_edge_index, self.num_nodes
        )
        self.out_degrees = calculate_out_degrees(self.g_edge_index, self.num_nodes)

    def fast_ppr(self, s, t):
        t_set, f_set, pi_inv = self.frontier(t)

        # print(f_set)
        # print(t_set)

        if s in t_set:
            return pi_inv[s]

        target_num_walks = int(self.c * self.e_rev / self.delta)
        # print(target_num_walks)
        hit_nodes = []
        for _ in range(target_num_walks):
            walk = geometric_random_walk(
                self.g_edge_index, self.num_nodes, self.alpha, s
            )
            for n in walk:
                # print("Hello :D", n)
                if n in f_set:
                    hit_nodes.append(n)
                    # print("Hello :D", n)
                    # print(hit_nodes)
                    # assert False
                    break
        # print(hit_nodes)

        result = torch.Tensor([0.0])
        for n in hit_nodes:
            result += pi_inv[n]
        result /= target_num_walks

        return result.item()
        ...

    def frontier(self, t):
        e_inv = self.beta * self.e_rev

        estimate_vector = torch.zeros([self.num_nodes])
        estimate_vector[t] = self.alpha
        residual_vector = estimate_vector.clone()

        target_set = {t}
        frontier_set = set(self.incoming_neighbors[t])

        while True:
            w = torch.argmax(residual_vector).item()
            if residual_vector[w] <= self.alpha * e_inv:
                # print("break:", residual_vector[w])
                # print("self.alpha * e_inv: ", self.alpha, " * ", e_inv)
                break
            # print("len(self.incoming_neighbors[w])", len(self.incoming_neighbors[w]))
            for u in self.incoming_neighbors[w]:

                big_d = (1 - self.alpha) * residual_vector[w] / self.out_degrees[u]
                # print("self.out_degrees[u]:", self.out_degrees[u])
                # print("big_d:", big_d)
                estimate_vector[u] += big_d
                residual_vector[u] += big_d
                # print("estimate_vector[u]:", estimate_vector[u], "e_rev:", self.e_rev)
                if estimate_vector[u] > self.e_rev:
                    target_set.add(u)
                    frontier_set.update(self.incoming_neighbors[u])
            residual_vector[w] = 0

        # print("len(target_set):", len(target_set))

        frontier_set = frontier_set - target_set

        print("len(target_set):", len(target_set))
        print("len(frontier_set):", len(frontier_set))

        return target_set, frontier_set, residual_vector


G_nk = nk.readGraph("./foodweb-baydry.konect")
n = G_nk.numberOfNodes()
is_undir = not G_nk.isDirected()
G_pyg = from_networkit(G_nk)[0]
G_pyg = EdgeIndex(G_pyg, sparse_size=(n, n), is_undirected=is_undir)


for alpha in [0.1, 0.3, 0.5]:

    print("--------------------------------")

    delta = 1 / n
    print("delta:", delta)
    print("alpha:", alpha)

    my_ppr = PPR(G_pyg, alpha=alpha, delta=delta)

    random.seed(0)
    for _ in range(20):
        print("----------------")
        s = random.randint(0, n - 1)
        t = s
        while t == s:
            t = random.randint(0, n - 1)

        print(s, t)
        result = my_ppr.fast_ppr(s, t)
        print(result)

# s = 0
# t = 56#n - 2#56


...

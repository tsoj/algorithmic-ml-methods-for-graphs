import torch
import itertools


def to_google_matrix(matrix, dampening_factor=0.5):
    N = len(matrix)  # Number of nodes
    stochastic_matrix = torch.tensor(matrix, dtype=torch.float32)
    # Make the matrix column stochastic
    column_sums = torch.sum(stochastic_matrix, dim=0)
    stochastic_matrix = torch.div(stochastic_matrix, column_sums)
    # Apply dampening factor
    google_matrix = dampening_factor * stochastic_matrix
    # Add teleportation matrix
    teleportation_matrix = (1 - dampening_factor) * torch.ones((N, N)) / N
    google_matrix += teleportation_matrix
    return google_matrix


# Function to calculate eigenvectors of a matrix
def calculate_eigenvectors(matrix):
    eigenvalues, eigenvectors = torch.linalg.eig(matrix)
    return eigenvalues, eigenvectors

def compare_eigenvectors(eigenvalues1, eigenvectors1, eigenvalues2, eigenvectors2):
    close_to_one = 1e-2  # Threshold for eigenvalue to be considered close to 1.0
    for i, val in enumerate(eigenvalues1):
        if abs(val.real - 1.0) < close_to_one:
            # Compare eigenvectors
            vec1 = eigenvectors1[:, i]
            for j, val2 in enumerate(eigenvalues2):
                if abs(val2.real - 1.0) < close_to_one:
                    vec2 = eigenvectors2[:, j]
                    if torch.allclose(vec1, vec2, atol=1e-2):
                        print("Eigenvectors are close for eigenvalue near 1.0.")
                        print(vec1, val)
                        print(vec2, val2)


# Generate all combinations of 3x3 matrices with 0.0 and 1.0
combinations = list(itertools.product([0.0, 1.0], repeat=9))
matrices = [list(zip(*[iter(comb)] * 3)) for comb in combinations]

# Calculate eigenvectors for each matrix
eigenvectors_list = []
for matrix in matrices:
    original_matrix = torch.tensor(matrix, dtype=torch.float32)
    google_matrix = to_google_matrix(matrix)
    if google_matrix.isnan().any():
        continue
    eigenvalues_orig, eigenvectors_orig = calculate_eigenvectors(original_matrix)
    eigenvalues_google, eigenvectors_google = calculate_eigenvectors(google_matrix)
    print("--------------------------------------")
    print(matrix)
    print(google_matrix)
    compare_eigenvectors(eigenvalues_orig, eigenvectors_orig, eigenvalues_google, eigenvectors_google)


#     google_matrix = to_google_matrix(matrix)
#     if google_matrix.isnan().any():
#         continue
#     eigenvectors = calculate_eigenvectors(google_matrix)
#     eigenvectors_list.append(eigenvectors)

# # Output the eigenvectors for each matrix
# for i, eigenvectors in enumerate(eigenvectors_list):
#     print(f"Matrix {i+1}:")
#     print(torch.tensor(matrices[i]))
#     print("Eigenvectors:")
#     print(eigenvectors)
#     print()
